import io.javalin.Javalin

fun main() {

    val app = Javalin.create()
    app.ws("/websocket/:path") { ws ->
        ws.onConnect { println("Connected to server") }
        ws.onClose { println("Disconnected from server.") }
    }.start("155.138.174.158", 8001)
}